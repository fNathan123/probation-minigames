using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace MSequence.Game
{
	public class MemoryButton : MonoBehaviour, IPointerDownHandler
	{
		public Action<int> ButtonEvent;
		public int id;
		public bool isActive;

		[Header("On Click Clip")] 
		[SerializeField] private AudioClip clip;

		private Animator _anim;
		private static readonly int IsClicked = Animator.StringToHash("isClicked");

		private void Start()
		{
			_anim = GetComponent<Animator>();
		}

		public void OnPointerDown(PointerEventData eventData)
		{
			if (isActive)
			{
				_anim.SetTrigger(IsClicked);
				ButtonEvent?.Invoke(id);
			}
		}

		public void ManualTrigger()
		{
			_anim.SetTrigger(IsClicked);
			ButtonEvent?.Invoke(id);
		}

		public AudioClip GetAudioClip()
		{
			return clip;
		}
	}
}