using System;
using System.Collections.Generic;
using UnityEngine;

namespace MSequence.Game
{
	public class MemoryButtonManager : MonoBehaviour
	{
		public static MemoryButtonManager Instance { get; private set; }

		[Header("Memory Buttons")] 
		[SerializeField] private List<MemoryButton> buttons;

		public event Action<int> ButtonClicked;

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			for (int i = 0; i < buttons.Count; i++)
			{
				buttons[i].id = i;
				buttons[i].ButtonEvent = (i) => { ButtonClicked?.Invoke(i); };
			}
		}

		private void OnDestroy()
		{
			ButtonClicked = null;
		}

		#region Button Manipulation

		public void ButtonClickTrigger(int id)
		{
			buttons[id].ManualTrigger();
		}

		public AudioClip GetButtonClip(int id)
		{
			AudioClip clip = buttons[id].GetAudioClip();
			return clip;
		}

		public void SetGameplayButtonResponsive(bool value)
		{
			foreach (var b in buttons)
			{
				b.isActive = value;
			}
		}

		#endregion
	}
}