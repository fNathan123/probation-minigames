using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;


namespace MSequence.Game
{
	public class GameplayManager : MonoBehaviour
	{
		public static GameplayManager Instance { get; private set; }
		public GameState State { get; private set; }

		private List<int> _questionSequence;
		private List<int> _userAnswer;

		public event Action GameOverHappened;
		public event Action GameStartHappened;

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			_questionSequence = new List<int>();
			_userAnswer = new List<int>();

			MemoryButtonManager.Instance.ButtonClicked += OnButtonClick;
		}

		private void OnDestroy()
		{
			GameOverHappened = null;
			GameStartHappened = null;

			MemoryButtonManager.Instance.ButtonClicked -= OnButtonClick;
		}

		#region Event Listener

		private void OnButtonClick(int id)
		{
			if (State == GameState.INPUTPHASE)
			{
				InsertAnswer(id);
			}
		}

		#endregion

		#region Gameplay Core

		private async void QuestionPhase()
		{
			State = GameState.QUESTIONPHASE;
			MemoryButtonManager.Instance.SetGameplayButtonResponsive(false);
			GenerateSequence();
			/*string d = "";
			for (int i = 0; i < _questionSequence.Count; i++)
			{
				d += _questionSequence[i] + ", ";
			}*/

			var isFinished = await PlaySequenceAsync();
			if (isFinished)
			{
				State = GameState.INPUTPHASE;
				_userAnswer.Clear();
				MemoryButtonManager.Instance.SetGameplayButtonResponsive(true);
			}
		}

		private void InsertAnswer(int id)
		{
			_userAnswer.Add(id);
			/*string d = "";
			for (int i = 0; i < _userAnswer.Count; i++)
			{
				d += _userAnswer[i] + ", ";
			}*/

			CheckAnswer();
		}

		private void CheckAnswer()
		{
			//State = GameState.CHECKINGPHASE;
			for (int i = 0; i < _userAnswer.Count; i++)
			{
				if (_questionSequence[i] != _userAnswer[i])
				{
					State = GameState.GAMEOVER;
					GameOver();
					return;
				}
			}

			if (_userAnswer.Count == _questionSequence.Count)
			{
				State = GameState.QUESTIONPHASE;
				QuestionPhase();
			}
		}

		private void GenerateSequence()
		{
			_questionSequence.Add(Random.Range(0, 4));
		}

		private async Task<bool> PlaySequenceAsync()
		{
			await Task.Delay(1500);
			for (int i = 0; i < _questionSequence.Count; i++)
			{
				//UIManager.Instance.ButtonClickTrigger(_questionSequence[i]);
				MemoryButtonManager.Instance.ButtonClickTrigger(_questionSequence[i]);
				await Task.Delay(1000);
			}

			return true;
		}

		#endregion

		#region Gameplay Manipulator

		public void StartGame()
		{
			QuestionPhase();
			//UIManager.Instance.SetActiveStartButton(false);
			GameStartHappened?.Invoke();
		}

		public void GameOver()
		{
			Debug.Log("Game Over");
			//UIManager.Instance.SetActiveGameOverUI(true);
			GameOverHappened?.Invoke();
		}

		public void Restart()
		{
			SceneManager.LoadScene("MemorySequence");
		}

		#endregion
	}
}