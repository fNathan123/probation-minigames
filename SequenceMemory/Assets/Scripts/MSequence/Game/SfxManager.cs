using UnityEngine;

namespace MSequence.Game
{
	public class SfxManager : MonoBehaviour
	{
		private AudioSource _audioSource;

		private void Start()
		{
			_audioSource = GetComponent<AudioSource>();
			MemoryButtonManager.Instance.ButtonClicked += OnButtonClicked;
		}

		private void OnDestroy()
		{
			MemoryButtonManager.Instance.ButtonClicked -= OnButtonClicked;
		}

		private void PlaySfx(AudioClip clip)
		{
			_audioSource.PlayOneShot(clip);
		}

		#region Event Listener

		private void OnButtonClicked(int id)
		{
			AudioClip clip = MemoryButtonManager.Instance.GetButtonClip(id);
			PlaySfx(clip);
		}

		#endregion
	}
}