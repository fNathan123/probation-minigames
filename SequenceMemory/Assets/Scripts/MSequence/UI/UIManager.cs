using MSequence.Game;
using UnityEngine;
using UnityEngine.UI;

namespace MSequence.UI
{
	public class UIManager : MonoBehaviour
	{
		[Header("UI Components")] 
		[SerializeField] private Button restartButton;
		[SerializeField] private Button startButton;
		[SerializeField] private Canvas gameOverUI;

		private void Start()
		{
			startButton.onClick.AddListener(StartButtonOnClick);
			restartButton.onClick.AddListener(RestartButtonOnClick);

			GameplayManager.Instance.GameStartHappened += OnGameStartHappened;
			GameplayManager.Instance.GameOverHappened += OnGameOverHappened;
		}

		private void OnDestroy()
		{
			GameplayManager.Instance.GameStartHappened -= OnGameStartHappened;
			GameplayManager.Instance.GameOverHappened -= OnGameOverHappened;
		}

		private void SetActiveGameOverUI(bool value)
		{
			gameOverUI.gameObject.SetActive(value);
		}

		private void SetActiveStartButton(bool value)
		{
			startButton.gameObject.SetActive(value);
		}

		#region Event Listener

		private void OnGameStartHappened()
		{
			SetActiveStartButton(false);
		}

		private void OnGameOverHappened()
		{
			SetActiveGameOverUI(true);
		}

		#endregion

		#region Button Listeners

		private void StartButtonOnClick()
		{
			GameplayManager.Instance.StartGame();
		}

		private void RestartButtonOnClick()
		{
			GameplayManager.Instance.Restart();
		}

		#endregion
	}
}