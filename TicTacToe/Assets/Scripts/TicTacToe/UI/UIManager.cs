using TicTacToe.Game;
using UnityEngine;
using UnityEngine.UI;

namespace TicTacToe.UI
{
	public class UIManager : MonoBehaviour
	{
		public static UIManager Instance { get; private set; }

		[SerializeField] Button restartButton;

		[SerializeField] Text winnerText;

		[SerializeField] Canvas gameOverUI;

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			//restartButton.buttonEvent = () => { GameplayManager.Instance.Restart(); };
			restartButton.onClick.AddListener(RestartOnClick);
			GameplayManager.Instance.GameOverEvent += PrepareGameOverUI;
		}

		private void PrepareGameOverUI(string winner)
		{
			UpdateWinnerText(winner);
			SetActiveGameOverUI(true);
		}

		public void UpdateWinnerText(string value)
		{
			winnerText.text = value + " WIN!";
		}

		public void SetActiveGameOverUI(bool value)
		{
			gameOverUI.gameObject.SetActive(value);
		}

		private void RestartOnClick()
		{
			GameplayManager.Instance.Restart();
		}
	}
}