using System;
using System.Collections.Generic;
using TicTacToe.Game;
using UnityEngine;

namespace TicTacToe.Game
{
	public class SlotButtonManager : MonoBehaviour
	{
		public static SlotButtonManager Instance { get; private set; }

		[SerializeField] private List<SlotButton> slotButtons;
		[SerializeField] private Sprite oShape;
		[SerializeField] private Sprite xShape;

		private void Awake()
		{
			Instance = this;
		}

		public void UpdateButtonUIShape(int id, int value)
		{
			Sprite spriteShape = oShape;
			if (value == 1)
			{
				spriteShape = xShape;
			}

			slotButtons[id].UpdateShape(spriteShape);
		}
	}
}