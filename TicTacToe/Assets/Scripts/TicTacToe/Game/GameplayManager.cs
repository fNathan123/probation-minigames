using System;
using System.Collections.Generic;
using TicTacToe.Game;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace TicTacToe.Game
{
	public class GameplayManager : MonoBehaviour
	{
		public static GameplayManager Instance { get; private set; }
		public GameState State { get; private set; }
		public event Action<string> GameOverEvent;
		public int[,] Board { get; private set; }

		Dictionary<int, (int, int)> _btnCoordinate;
		bool _isPlayerTurn;

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			Board = new int[3, 3];
			for (int i = 0; i < Board.GetLength(0); i++)
			{
				for (int j = 0; j < Board.GetLength(1); j++)
				{
					Board[i, j] = -1;
				}
			}

			_isPlayerTurn = CoinFlip();
			ChangeStateByTurn();

			//register btn coor
			_btnCoordinate = new Dictionary<int, (int, int)>();
			_btnCoordinate.Add(0, (0, 0));
			_btnCoordinate.Add(1, (0, 1));
			_btnCoordinate.Add(2, (0, 2));
			_btnCoordinate.Add(3, (1, 0));
			_btnCoordinate.Add(4, (1, 1));
			_btnCoordinate.Add(5, (1, 2));
			_btnCoordinate.Add(6, (2, 0));
			_btnCoordinate.Add(7, (2, 1));
			_btnCoordinate.Add(8, (2, 2));

			ClickHandler.Instance.RayHitEvent += InputShape;
		}

		public bool CoinFlip()
		{
			float rng = Random.Range(0, 1);
			return (rng > 0.5);
		}

		public void InputShape(int btnID)
		{
			(int, int) coor;
			_btnCoordinate.TryGetValue(btnID, out coor);
			if (Board[coor.Item1, coor.Item2] == -1 && State != GameState.GAMEOVER)
			{
				if (State == GameState.OTURN)
				{
					UpdateBoard(coor, btnID, 0);
				}
				else
				{
					UpdateBoard(coor, btnID, 1);
				}
			}
		}

		public void UpdateBoard((int, int) coor, int btnID, int value)
		{
			Board[coor.Item1, coor.Item2] = value;
			//UIManager.Instance.UpdateSlotImage(btnID, value);
			SlotButtonManager.Instance.UpdateButtonUIShape(btnID, value);
			_isPlayerTurn = !_isPlayerTurn;
			ChangeStateByTurn();
			CheckGameOver();
		}

		void ChangeStateByTurn()
		{
			if (_isPlayerTurn)
			{
				State = GameState.OTURN;
			}
			else
			{
				State = GameState.XTURN;
			}
		}

		void CheckGameOver()
		{
			int x;
			int o;
			//check horizontally
			for (int i = 0; i < Board.GetLength(0); i++)
			{
				x = 0;
				o = 0;
				for (int j = 0; j < Board.GetLength(1); j++)
				{
					if (Board[i, j] == 1)
					{
						x++;
					}
					else if (Board[i, j] == 0)
					{
						o++;
					}
				}

				if (x == 3 || o == 3)
				{
					if (x > o)
					{
						GameOver("X");
						return;
					}
					else
					{
						GameOver("O");
						return;
					}
				}
			}

			//check vertically
			for (int i = 0; i < Board.GetLength(1); i++)
			{
				x = 0;
				o = 0;
				for (int j = 0; j < Board.GetLength(0); j++)
				{
					if (Board[j, i] == 1)
					{
						x++;
					}
					else if (Board[j, i] == 0)
					{
						o++;
					}
				}

				if (x == 3 || o == 3)
				{
					if (x > o)
					{
						GameOver("X");
						return;
					}
					else
					{
						GameOver("O");
						return;
					}
				}
			}

			x = 0;
			o = 0;
			//check diagonally1
			for (int i = 0; i < Board.GetLength(0); i++)
			{
				if (Board[i, i] == 1)
				{
					x++;
				}
				else if (Board[i, i] == 0)
				{
					o++;
				}

				if (x == 3 || o == 3)
				{
					if (x > o)
					{
						GameOver("X");
						State = GameState.GAMEOVER;
						return;
					}
					else
					{
						GameOver("O");
						State = GameState.GAMEOVER;
						return;
					}
				}
			}

			x = 0;
			o = 0;
			//check diagonally2
			for (int i = Board.GetLength(0) - 1; i >= 0; i--)
			{
				if (Board[i, Board.GetLength(0) - 1 - i] == 1)
				{
					x++;
				}
				else if (Board[i, Board.GetLength(0) - 1 - i] == 0)
				{
					o++;
				}

				if (x == 3 || o == 3)
				{
					if (x > o)
					{
						GameOver("X");
						return;
					}
					else
					{
						GameOver("O");
						return;
					}
				}
			}

			//Check Tie
			for (int i = 0; i < Board.GetLength(0); i++)
			{
				for (int j = 0; j < Board.GetLength(1); j++)
				{
					if (Board[i, j] == -1)
					{
						return;
					}
				}
			}

			GameOver("NO ONE");
		}

		public void GameOver(string winner)
		{
			State = GameState.GAMEOVER;
			GameOverEvent?.Invoke(winner);
			//UIManager.Instance.UpdateWinnerText(winner);
			//UIManager.Instance.SetActiveGameOverUI(true);
		}

		public void Restart()
		{
			SceneManager.LoadScene("TicTacToe");
		}
	}
}