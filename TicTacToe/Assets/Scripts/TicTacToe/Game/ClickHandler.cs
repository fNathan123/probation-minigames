using System;
using UnityEngine;

namespace TicTacToe.Game
{
	public class ClickHandler : MonoBehaviour
	{
		public static ClickHandler Instance { get; private set; }
		public event Action<int> RayHitEvent;
		[SerializeField] private Camera mainCamera;

		private void Awake()
		{
			Instance = this;
		}

		private void Update()
		{
			if (Input.GetMouseButtonDown(0))
			{
				CheckRayHit();
			}
		}

		private void CheckRayHit()
		{
			RaycastHit2D rayHit = Physics2D.GetRayIntersection(mainCamera.ScreenPointToRay(Input.mousePosition));

			if (rayHit.transform)
			{
				SlotButton b = rayHit.transform.GetComponent<SlotButton>();
				RayHitEvent?.Invoke(b.id);
			}
		}
	}
}