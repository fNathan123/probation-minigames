using UnityEngine;
using UnityEngine.UI;

namespace TicTacToe.Game
{
	public class SlotButton : MonoBehaviour
	{
		public int id;
		private Image _image;

		private void Awake()
		{
			_image = GetComponent<Image>();
		}

		public void UpdateShape(Sprite shape)
		{
			_image.sprite = shape;
		}
	}
}