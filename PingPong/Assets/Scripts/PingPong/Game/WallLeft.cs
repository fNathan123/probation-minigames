using PingPong.Core;
using UnityEngine;

namespace PingPong.Game
{
	public class WallLeft : MonoBehaviour, IPongObject
	{
		public ObjectType ObjType => ObjectType.GOALLEFT;
	}
}