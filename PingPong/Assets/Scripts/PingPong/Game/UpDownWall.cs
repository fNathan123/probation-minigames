using PingPong.Core;
using UnityEngine;

namespace PingPong.Game
{
	public class UpDownWall : MonoBehaviour, IPongObject
	{
		public ObjectType ObjType => ObjectType.UPDOWNWALL;
	}
}