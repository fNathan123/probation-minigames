using System;
using PingPong.Game;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PingPong.Game
{
	public class GameplayManager : MonoBehaviour
	{
		public static GameplayManager Instance { get; private set; }

		[Header("Game Objects")] [SerializeField]
		private Paddle paddle1;

		[SerializeField] private Paddle paddle2;
		[SerializeField] private Ball ball;

		public GameState State { get; private set; }
		public int Player1Score { get; private set; }
		public int Player2Score { get; private set; }

		public event Action<int> GameOverHappened;
		public event Action<int, int> ScoreChanged;

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			State = GameState.PLAYING;

			ball.GoalHappened += OnGoalHappened;
		}

		private void OnDestroy()
		{
			ball.GoalHappened -= OnGoalHappened;
			GameOverHappened = null;
			ScoreChanged = null;
		}

		#region EventListener

		private void OnGoalHappened(int player)
		{
			if (player == 1)
			{
				Player1Score++;
				ScoreChanged?.Invoke(1, Player1Score);
			}
			else if (player == 2)
			{
				Player2Score++;
				ScoreChanged?.Invoke(2, Player2Score);
			}

			ResetPosition();
			if (Player1Score == 3 || Player2Score == 3)
			{
				int winner;
				if (Player1Score == 3)
				{
					winner = 1;
				}
				else
				{
					winner = 2;
				}

				GameOver(winner);
			}
		}

		#endregion

		public void ResetPosition()
		{
			paddle1.transform.position = paddle1.GetComponent<Paddle>().InitPosition;
			paddle2.transform.position = paddle2.GetComponent<Paddle>().InitPosition;
			ball.transform.position = ball.GetComponent<Ball>().InitPosition;
			ball.GetComponent<Ball>().GoToRandomDirection();
		}

		#region Gameplay Manipulation

		public void RestartGame()
		{
			SceneManager.LoadScene("PingPong");
		}

		public void GameOver(int winningPlayer)
		{
			State = GameState.GAMEOVER;
			GameOverHappened?.Invoke(winningPlayer);
			ball.Stop();
			paddle1.Stop();
			paddle2.Stop();
		}

		#endregion
	}
}