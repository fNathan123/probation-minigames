using PingPong.Core;
using UnityEngine;

namespace PingPong.Game
{
	public class WallRight : MonoBehaviour, IPongObject
	{
		public ObjectType ObjType => ObjectType.GOALRIGHT;
	}
}