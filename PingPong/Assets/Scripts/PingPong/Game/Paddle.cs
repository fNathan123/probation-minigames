using PingPong.Core;
using UnityEngine;

namespace PingPong.Game
{
	public class Paddle : MonoBehaviour, IPongObject
	{
		public ObjectType ObjType => ObjectType.PADDLE;

		public Vector3 InitPosition { get; private set; }

		[Header("Attributes")] 
		[SerializeField] private int player;
		[SerializeField] private float speed;

		private float _ySpeed;

		private KeyCode _upCode;
		private KeyCode _downCode;

		private void Start()
		{
			InitPosition = transform.position;

			if (player == 1)
			{
				_upCode = KeyCode.W;
				_downCode = KeyCode.S;
			}
			else if (player == 2)
			{
				_upCode = KeyCode.UpArrow;
				_downCode = KeyCode.DownArrow;
			}
		}

		private void Update()
		{
			if (Input.GetKeyDown(_upCode))
			{
				_ySpeed = speed;
			}

			if (Input.GetKeyDown(_downCode))
			{
				_ySpeed = -1 * speed;
			}

			if (Input.GetKeyUp(_upCode) || Input.GetKeyUp(_downCode))
			{
				_ySpeed = 0;
			}
		}

		private void FixedUpdate()
		{
			transform.position += Vector3.up * (_ySpeed * Time.deltaTime);
		}

		public void Stop()
		{
			speed = 0;
		}
	}
}