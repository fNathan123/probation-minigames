using System;
using PingPong.Core;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PingPong.Game
{
	public class Ball : MonoBehaviour, IPongObject
	{
		[Header("Attribute")] 
		[SerializeField] private float speed;
		[SerializeField] private float maxAngle;

		public ObjectType ObjType => ObjectType.BALL;
		public Vector3 InitPosition { get; private set; }

		public event Action<int> GoalHappened;

		private float _xSpeed;
		private float _ySpeed;

		private void Start()
		{
			GoToRandomDirection();
		}

		private void OnDestroy()
		{
			GoalHappened = null;
		}

		private float RandomDirection()
		{
			float rng = Random.Range(0f, 1f);
			if (rng > 0.5f)
			{
				return 1;
			}
			else
			{
				return -1;
			}
		}

		private void FixedUpdate()
		{
			transform.position += Vector3.right * (_xSpeed * Time.deltaTime) + Vector3.up * (_ySpeed * Time.deltaTime);
		}

		private void OnCollisionEnter2D(Collision2D collision)
		{
			IPongObject colObj = collision.gameObject.GetComponent<IPongObject>();
			if (colObj != null)
			{
				if (colObj.ObjType == ObjectType.UPDOWNWALL)
				{
					_ySpeed *= -1;
				}
				else if (colObj.ObjType == ObjectType.GOALLEFT)
				{
					GoalHappened?.Invoke(2);
				}
				else if (colObj.ObjType == ObjectType.GOALRIGHT)
				{
					GoalHappened?.Invoke(1);
				}
				else if (colObj.ObjType == ObjectType.PADDLE)
				{
					ContactPoint2D contact = collision.contacts[0];
					SpriteRenderer paddleRenderer = collision.gameObject.GetComponent<SpriteRenderer>();
					float paddleHeight = paddleRenderer.sprite.rect.height;
					float paddlePosY = collision.gameObject.transform.position.y;

					float collisionRelative = contact.point.y - paddlePosY;
					float normalizeCRelative = collisionRelative / ((paddleHeight / 2) / 100);
					//Debug.Log("half height: " + ((paddleHeight / 2)/100) + " collision relative: " + collisionRelative);
					//Debug.Log("normalized :" + normalizeCRelative);
					float bounceAngle = normalizeCRelative * maxAngle;

					Vector3 bounceDirection = Quaternion.AngleAxis(bounceAngle, Vector3.forward) * transform.right;

					_ySpeed = bounceDirection.y * speed;
					_xSpeed = (_xSpeed / Mathf.Abs(_xSpeed) * -1) * bounceDirection.x * speed;
				}
			}
		}

		public void GoToRandomDirection()
		{
			InitPosition = transform.position;
			Vector3 initDirection = Quaternion.AngleAxis(Random.Range(0f, 81f), Vector3.forward) * transform.right *
			                        RandomDirection();
			_xSpeed = speed * initDirection.x;
			_ySpeed = speed * initDirection.y;
		}

		public void Stop()
		{
			_xSpeed = 0;
			_ySpeed = 0;
		}
	}
}