namespace PingPong.Core
{
	public interface IPongObject
	{
		public ObjectType ObjType { get; }
	}
}