using PingPong.Game;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PingPong.UI
{
	public class UIManager : MonoBehaviour
	{
		[SerializeField] private TMP_Text player1ScoreText;
		[SerializeField] private TMP_Text player2ScoreText;
		[SerializeField] private TMP_Text winningPlayerText;
		[SerializeField] private Canvas gameOverUI;

		[SerializeField] private Button restartButton;

		private void Awake()
		{
			restartButton.onClick.AddListener(RestartOnClick);
		}

		private void Start()
		{
			GameplayManager.Instance.ScoreChanged += OnScoreChanged;
			GameplayManager.Instance.GameOverHappened += OnGameOverHappened;
		}

		#region Event listener

		private void OnScoreChanged(int player, int value)
		{
			UpdateScore(player, value);
		}

		private void OnGameOverHappened(int winner)
		{
			UpdateWinningPlayer(winner);
			SetActiveGameOverUI(true);
		}

		#endregion

		public void UpdateScore(int player, int value)
		{
			if (player == 1)
			{
				player1ScoreText.text = value.ToString();
			}
			else
			{
				player2ScoreText.text = value.ToString();
			}
		}

		public void UpdateWinningPlayer(int player)
		{
			winningPlayerText.text = "Player " + player + " Wins!";
		}

		public void SetActiveGameOverUI(bool value)
		{
			gameOverUI.gameObject.SetActive(value);
		}

		public void RestartOnClick()
		{
			GameplayManager.Instance.RestartGame();
		}
	}
}