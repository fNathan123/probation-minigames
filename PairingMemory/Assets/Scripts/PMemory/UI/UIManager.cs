using PMemory.Game;
using UnityEngine;
using UnityEngine.UI;

namespace PMemory.UI
{
	public class UIManager : MonoBehaviour
	{
		[Header("UI Components")] 
		[SerializeField] private Button restartButton;
		[SerializeField] private Canvas gameOverUI;

		private void Start()
		{
			restartButton.onClick.AddListener(RestartOnClick);
			GameplayManager.Instance.GameOverHappened += OnGameOverHappened;
		}

		private void OnDestroy()
		{
			GameplayManager.Instance.GameOverHappened -= OnGameOverHappened;
		}

		private void SetActiveGameOverUI(bool value)
		{
			gameOverUI.gameObject.SetActive(value);
		}

		#region Event Listener

		private void OnGameOverHappened()
		{
			SetActiveGameOverUI(true);
		}

		#endregion

		#region Button onclick

		private void RestartOnClick()
		{
			GameplayManager.Instance.Restart();
		}

		#endregion
	}
}