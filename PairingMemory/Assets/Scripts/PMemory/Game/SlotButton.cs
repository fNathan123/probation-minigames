using UnityEngine;
using UnityEngine.UI;

namespace PMemory.Game
{
	public class SlotButton : MonoBehaviour
	{
		public int id;

		private Image _image;

		private void Awake()
		{
			_image = GetComponent<Image>();
		}

		public void ChangeColor(Color color)
		{
			_image.color = new Color(color.r, color.g, color.b);
		}
	}
}