using System;
using UnityEngine;

namespace PMemory.Game
{
	public class InputClickHandler : MonoBehaviour
	{
		public static InputClickHandler Instance { get; private set; }

		public event Action<int> RayHit;

		[SerializeField] private Camera mainCamera;

		private void Awake()
		{
			Instance = this;
		}

		private void OnDestroy()
		{
			RayHit = null;
		}

		void Update()
		{
			if (Input.GetMouseButtonDown(0))
			{
				CheckRayHit();
			}
		}

		private void CheckRayHit()
		{
			RaycastHit2D hit = Physics2D.GetRayIntersection(mainCamera.ScreenPointToRay(Input.mousePosition));

			if (hit.transform)
			{
				SlotButton b = hit.transform.GetComponent<SlotButton>();
				RayHit?.Invoke(b.id);
			}
		}
	}
}