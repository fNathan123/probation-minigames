using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;


namespace PMemory.Game
{
	public class GameplayManager : MonoBehaviour
	{
		public static GameplayManager Instance { get; private set; }

		[SerializeField] private List<Color> colorPool;
		public GameState State { get; private set; }

		public event Action GameOverHappened;

		private List<(int, SlotInformation)> _playerAnswer;
		private Dictionary<int, SlotInformation> _board;
		private int _correctAnswer;

		private void Awake()
		{
			Instance = this;
		}

		private void Start()
		{
			_board = new Dictionary<int, SlotInformation>();
			_playerAnswer = new List<(int, SlotInformation)>();

			List<int> boardID = new List<int>();
			for (int i = 0; i < colorPool.Count * 2; i++)
			{
				boardID.Add(i);
			}

			boardID = Shuffle(boardID);

			int idx = 0;
			for (int i = 0; i < colorPool.Count; i++)
			{
				SlotInformation slot = new SlotInformation(colorPool[i]);
				_board.Add(boardID[idx], slot);
				_board.Add(boardID[idx + 1], slot);
				idx += 2;
			}

			//Listening
			InputClickHandler.Instance.RayHit += OnRayHit;

			State = GameState.PICKPHASE;
		}

		private void OnDestroy()
		{
			GameOverHappened = null;

			InputClickHandler.Instance.RayHit -= OnRayHit;
		}

		#region Event Listener

		private void OnRayHit(int id)
		{
			FlipSlot(id);
		}

		#endregion

		#region Gameplay core

		public void FlipSlot(int id)
		{
			if (State == GameState.PICKPHASE)
			{
				if (_playerAnswer.Count < 2)
				{
					_board.TryGetValue(id, out var slot);
					if (slot != null)
					{
						SlotButtonManager.Instance.ChangeButtonColor(id, slot.Color);
					}

					_playerAnswer.Add((id, slot));
					if (_playerAnswer.Count == 2)
					{
						State = GameState.REVEALPHASE;
						StartCoroutine(Reveal());
					}
				}
			}
		}

		public List<int> Shuffle(List<int> set)
		{
			System.Random rng = new System.Random();
			List<int> shuffledSet = set.OrderBy(a => rng.Next()).ToList();
			return shuffledSet;
		}


		private IEnumerator Reveal()
		{
			yield return new WaitForSeconds(3f);
			SlotButtonManager.Instance.RevertAllButtonColor();
			if (_playerAnswer[0].Item2.Color == _playerAnswer[1].Item2.Color)
			{
				SlotButtonManager.Instance.DisableButton(_playerAnswer[0].Item1);
				SlotButtonManager.Instance.DisableButton(_playerAnswer[1].Item1);
				_correctAnswer += 2;
			}

			_playerAnswer.Clear();
			if (_correctAnswer == colorPool.Count * 2)
			{
				State = GameState.GAMEOVER;
				GameOver();
			}

			State = GameState.PICKPHASE;
		}

		#endregion

		#region Gameplay manipulation

		private void GameOver()
		{
			GameOverHappened?.Invoke();
		}

		public void Restart()
		{
			SceneManager.LoadScene("PairingMemory");
		}

		#endregion
	}

	public class SlotInformation
	{
		public Color Color;

		public SlotInformation(Color color)
		{
			this.Color = color;
		}
	}
}