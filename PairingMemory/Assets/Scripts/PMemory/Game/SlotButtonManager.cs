using System.Collections.Generic;
using UnityEngine;

namespace PMemory.Game
{
	public class SlotButtonManager : MonoBehaviour
	{
		public static SlotButtonManager Instance { get; private set; }

		[SerializeField] private List<SlotButton> buttons;

		private void Awake()
		{
			Instance = this;
			AssignButtonID();
		}

		private void AssignButtonID()
		{
			for (int i = 0; i < buttons.Count; i++)
			{
				buttons[i].id = i;
			}
		}

		public void RevertAllButtonColor()
		{
			foreach (SlotButton button in buttons)
			{
				button.ChangeColor(new Color(1, 1, 1));
			}
		}

		public void ChangeButtonColor(int id, Color color)
		{
			buttons[id].ChangeColor(new Color(color.r, color.g, color.b));
		}

		public void DisableButton(int id)
		{
			buttons[id].gameObject.SetActive(false);
		}
	}
}